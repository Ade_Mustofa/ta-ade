<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	var $header = 'dosen/header';

	public function __construct(){
	    parent::__construct();

	    $level = $this->session->userdata('level');

		if ($level === FALSE) {
			redirect('auth');
		} else if ($level === 'admin') {
			redirect('admin');
		} else if ($level === 'mahasiswa') {
			redirect('mahasiswa');
		}
	   
	  }


	public function index()
	{
		
		$data['header'] = $this->header;
		$data['page']	= 'dosen/index';
		$this->load->view('layout/app', $data);
	}
}
