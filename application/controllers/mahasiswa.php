<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	var $header = 'mahasiswa/header';
	public function __construct()
	{
	    parent::__construct();
	   
	   $level = $this->session->userdata('level');

		if ($level === FALSE) {
			redirect('auth');
		} else if ($level === 'admin') {
			redirect('admin');
		} else if ($level === 'dosen') {
			redirect('dosen');
		}

		$this->load->library('session');
		$this->load->model('users_model','',TRUE);
		$this->load->library('form_validation');
	}


	public function index()
	{
		$data['user']	= $this->session->userdata('username');
		$data['header'] = $this->header;
		$data['page']	= 'mahasiswa/index';
		$this->load->view('layout/app', $data);
	}

	public function tugas_akhir()
	{
		
		$data['user']	= $this->session->userdata('username');
		$data['header'] = $this->header;
		$data['page']	= 'mahasiswa/tugas_akhir';
		$this->load->view('layout/app', $data);
	}

	public function ppi()
	{
	
		$data['header'] = $this->header;
		$data['page']	= 'mahasiswa/ppi';
		$this->load->view('layout/app', $data);
	}

	public function post_upload()
	{

		$rules = array(
			array(
				'field' => 'nama',
				'label' => 'Submission',
				'rules' => 'require|trim'
				),
			array(
				'field' => 'pembimbing1',
				'label' => 'Pembimbing1',
				'rules' => 'require|trim'
				),
			array(
				'field' => 'pembimbing2',
				'label' => 'Pembimbing2',
				'rules' => 'require|trim'
				),

			);

		$data_doc = array(
			'nama' => $this->input->post('nama'),
			'pembimbing1' => $this->input->post('pembimbing1'),
			'pembimbing2' => $this->input->post('pembimbing2'),
			'create_at' => date("Y-m-d H:i:s")
			);

		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
			$this->tugas_akhir();
		} else {
			$this->mahasiswa_model->get_post('document', $data_doc);
			redirect('tugas_akhir');
		}
	}
}
