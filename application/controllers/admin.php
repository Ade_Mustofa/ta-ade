<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	var $header = 'admin/header';

	public function __construct(){
	    parent::__construct();

	    $level = $this->session->userdata('level');

		if ($level === FALSE) {
			redirect('auth');
		} else if ($level === 'dosen') {
			redirect('dosen');
		} else if ($level === 'mahasiswa') {
			redirect('mahasiswa');
		}

	   $this->load->model('admin_model','',TRUE);     	
	  }


	public function index()
	{
		$data['header'] = $this->header;
		$data['page']	= 'admin/index';
		$this->load->view('layout/app', $data);
	}

	public function user()
	{
		$data['users']	= $this->admin_model->get_user();
		$data['header'] = $this->header;
		$data['page']	= 'admin/user';
		$this->load->view('layout/app', $data);
	}
}
