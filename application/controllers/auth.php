<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	var $header = 'header';

	public function __construct(){
	    parent::__construct();

	    $this->load->library('session');
	    $this->load->model('users_model','',TRUE);
	  }


	public function index()
	{
		$level = $this->session->userdata('level');

		if ($level === 'admin') {
			redirect('admin');
		} else if ($level === 'dosen') {
			redirect('dosen');
		} else if ($level === 'mahasiswa') {
			redirect('mahasiswa');
		}
		$data['header'] = $this->header;
		$data['page'] = 'form_login';
		$this->load->view('layout/app', $data);
	}

	public function cek_login()
	{

	$config_rules = array(
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|trim',
				'errors' => array(
                        'required' => '%s tidak boleh kosong',
                ),
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|trim',
				'errors' => array(
                        'required' => '%s tidak boleh kosong',
                ),
			),
		);

		$this->form_validation->set_rules($config_rules);

		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {

			$data = array('username' => $this->input->post('username', TRUE),
					  'password' => md5($this->input->post('password', TRUE))
			);

		$hasil = $this->users_model->cek_user($data);

		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['id'] = $sess->id;
				$sess_data['username'] = $sess->username;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}

			$level = $this->session->userdata('level');
			switch ($level) {
				case 'admin':
					redirect('admin');
					break;
				case 'dosen':
					redirect('dosen');
					break;
				case 'mahasiswa':
					redirect('mahasiswa');
					break;
				default:
					redirect('auth');
					break;
			}
		}
		else {	
			echo "<script>alert('Gagal login: Cek username, password!', 'error');history.go(-1);</script>";
		}
		}

		
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('level');
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('welcome');
	}
}
