<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	var $header = 'header';

	public function index()
	{
		$data['header'] = 'header';
		$data['page']	= 'welcome_message';
		$this->load->view('layout/app', $data);
	}
}
