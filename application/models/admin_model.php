<?php
class Admin_model extends CI_Model{

	public function __construct(){
	    $this->load->database();
	  }

	public function get_user() {

		$this->db->from('users');
	    $query = $this->db->get();
	    return $query->result_array();
  	}
}
?>