<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<h3 class="daftar-user">Daftar User</h3>
		<div class="tabel-user">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Username</th>
						<th>Level</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $user) { ?>
					<tr>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['level']; ?></td>
					</tr>
				<?php } ?> 
				</tbody>
			</table>
		</div>
	</div>
</div>
