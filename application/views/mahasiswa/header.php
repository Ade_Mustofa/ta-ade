 <nav class="navbar">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      	<a class="navbar-brand" href="<?php echo base_url('mahasiswa/index'); ?>" data-toggle="tooltip" title="Home"><span class="glyphicon glyphicon-home"></span></a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="<?php echo base_url('mahasiswa/tugas_akhir'); ?>">Tugas Akhir</a></li>
	      <li><a href="<?php echo base_url('mahasiswa/ppi'); ?>">PPI</a></li>
	      <li><a href="<?php echo base_url('mahasiswa/ppi'); ?>">Riwayat</a></li>
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	      <li><a href="<?php echo base_url('auth/logout'); ?>"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
	    </ul>
	  </div>
	</nav>	