
<div class="container">
<div class="jumbotron">
	<h3>Pilih Submission untuk Bimbingan Laporan :</h3>
	&nbsp
<div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>Submission</th>
		<th>View Submission</th>
        <th>Info Submission</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="width: 600px;">
        	<div class="panel panel-warning">
		      	<div class="panel-heading">
		        	<h4 class="panel-title">
		          		<a data-toggle="collapse" href="#collapse1">BAB I</a>
		        	</h4>
		      	</div>
		    	<div id="collapse1" class="panel-collapse collapse">
			        <div class="panel-body" id="panel-bab1">
			        	<!-- <form id="signupform" class="form-horizontal" role="form"> -->
                           <?php echo form_open('mahasiswa/post_upload',array(
                                'method' => 'POST',
                                'class'  => 'form-horizontal'
                            )); ?>
                                <div class="form-group" hidden>
                                    <label for="nama" class="col-md-3 control-label">Submission</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nama" value="Bab 1">
                                    </div>
                                     <?php echo form_error('nama'); ?>
                                </div>    

                                <div class="form-group">
                                    <label for="Pembimbing1" class="col-md-3 control-label">Pebimbing 1</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="pembimbing1" placeholder="Pilih Pembimbing 1" value="<?php echo set_value("pembimbing1"); ?>">
                                        <?php echo form_error('pembimbing1'); ?>
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="Pembimbing2" class="col-md-3 control-label">Pembimbing 2</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="pembimbing2" placeholder="Pilih Pembimbing 2" value="<?php echo set_value("pembimbing2"); ?>">
                                        <?php echo form_error('pembimbing2'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Submission</label>
                                   	<div class="col-md-8 col-md-offset-3">
                                   		<input type="file" hidden>
                                   	</div>
                                </div>
                                    

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-8">
                                        <button href="<?php echo base_url('mahasiswa/post_upload'); ?>" class="btn btn-default">Submit</button> 
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                        <!-- </form> -->        
			        </div>
		    	</div>
		    </div>
        </td>
        <td><button class="btn btn-success">View</button></td>
        <td><button class=" btn btn-info" data-toggle="modal" href="#myModal">View Info</button></td>
        <td>Draft</td>
      </tr>
      <tr>
        <td style="width: 600px;">
        	<div class="panel panel-warning">
		      	<div class="panel-heading">
		        	<h4 class="panel-title">
		          		<a data-toggle="collapse" href="#collapse2">BAB II</a>
		        	</h4>
		      	</div>
		    	<div id="collapse2" class="panel-collapse collapse">
			        <div class="panel-body">
			        	<form id="signupform" class="form-horizontal" role="form">

                                <div class="form-group" hidden>
                                    <label for="email" class="col-md-3 control-label">Submission</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nama" value="Bab 2">
                                    </div>
                                </div>
                                   
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Pebimbing 1</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">Pembimbing 2</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Submission</label>
                                   	<div class="col-md-8 col-md-offset-3">
                                   		<input type="file" hidden>
                                   	</div>
                                </div>
                                    

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-8">
                                        <button id="btn-signup" type="button" class="btn btn-default"><i class="icon-hand-right"></i>Submit</button> 
                                    </div>
                                </div>
                        </form>        
			        </div>
		    	</div>
		    </div>
        </td>
        <td><button class=" btn btn-success">View</button></td>
        <td><button class=" btn btn-info">View Info</button></td>
        <td>Draft</td>
      </tr>
      <tr>
        <td style="width: 600px;">
        	<div class="panel panel-warning">
		      	<div class="panel-heading">
		        	<h4 class="panel-title">
		          		<a data-toggle="collapse" href="#collapse3">BAB III</a>
		        	</h4>
		      	</div>
		    	<div id="collapse3" class="panel-collapse collapse">
			        <div class="panel-body">
			        	<form id="signupform" class="form-horizontal" role="form">

                                <div class="form-group" hidden>
                                    <label for="email" class="col-md-3 control-label">Submission</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nama" value="Bab 3">
                                    </div>
                                </div>
                                   
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Pebimbing 1</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">Pembimbing 2</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Submission</label>
                                   	<div class="col-md-8 col-md-offset-3">
                                   		<input type="file" hidden>
                                   	</div>
                                </div>
                                    

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-8">
                                        <button id="btn-signup" type="button" class="btn btn-default"><i class="icon-hand-right"></i>Submit</button> 
                                    </div>
                                </div>
                        </form>        
			        </div>
		    	</div>
		    </div>
        </td>
        <td><button class=" btn btn-success">View</button></td>
        <td><button class=" btn btn-info">View Info</button></td>
        <td>Draft</td>
      </tr>
      <tr>
        <td style="width: 600px;">
        	<div class="panel panel-warning">
		      	<div class="panel-heading">
		        	<h4 class="panel-title">
		          		<a data-toggle="collapse" href="#collapse4">BAB IV</a>
		        	</h4>
		      	</div>
		    	<div id="collapse4" class="panel-collapse collapse">
			        <div class="panel-body">
			        	<form id="signupform" class="form-horizontal" role="form">

                                <div class="form-group" hidden>
                                    <label for="email" class="col-md-3 control-label">Submission</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nama" value="Bab 4">
                                    </div>
                                </div>
                                   
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Pebimbing 1</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">Pembimbing 2</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Submission</label>
                                   	<div class="col-md-8 col-md-offset-3">
                                   		<input type="file" hidden>
                                   	</div>
                                </div>
                                    

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-8">
                                        <button id="btn-signup" type="button" class="btn btn-default"><i class="icon-hand-right"></i>Submit</button> 
                                    </div>
                                </div>
                        </form>        
			        </div>
		    	</div>
		    </div>
        </td>
        <td><button class=" btn btn-success">View</button></td>
        <td><button class=" btn btn-info">View Info</button></td>
        <td>Draft</td>
      </tr>
      <tr>
        <td style="width: 600px;">
        	<div class="panel panel-warning">
		      	<div class="panel-heading">
		        	<h4 class="panel-title">
		          		<a data-toggle="collapse" href="#collapse5">BAB V</a>
		        	</h4>
		      	</div>
		    	<div id="collapse5" class="panel-collapse collapse">
			        <div class="panel-body">
			        	<form id="signupform" class="form-horizontal" role="form">

                                <div class="form-group" hidden>
                                    <label for="email" class="col-md-3 control-label">Submission</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nama" value="Bab 5">
                                    </div>
                                </div>
                                   
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Pebimbing 1</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">Pembimbing 2</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Submission</label>
                                   	<div class="col-md-8 col-md-offset-3">
                                   		<input type="file" hidden>
                                   	</div>
                                </div>
                                    

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-8">
                                        <button id="btn-signup" type="button" class="btn btn-default"><i class="icon-hand-right"></i>Submit</button> 
                                    </div>
                                </div>
                        </form>        
			        </div>
		    	</div>
		    </div>
        </td>
        <td><button class=" btn btn-success">View</button></td>
        <td><button class=" btn btn-info">View Info</button></td>
        <td>Draft</td>
      </tr>
    </tbody>
  </table>
  </div>

<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Info BAB I</h4>
			      </div>
			      <div class="modal-body">
			      <table class="table">
			      	<thead>
			      		<tr>
			      			<th>Pembimbing 1</th>
			      			<th>Pembimbing 2</th>
			      		</tr>
			      	</thead>
			      	<tbody>
			      		<tr>
			      			<td>A.Lubis Ghozali</td>
			      			<td>Eka ismantohadi</td>
			      		</tr>
			      	</tbody>
			      </table> 
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>

			  </div>
			</div>
</div>		
</div>
